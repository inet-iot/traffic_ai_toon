import cv2
import numpy as np
from ultralytics import YOLO
import json
from flask import Flask, request, jsonify

# โหลดโมเดล YOLOv8n
model = YOLO('yolov8n.pt')

app = Flask(__name__)


# ฟังก์ชั่นสำหรับการทำ Vehicle Tracking
def vehicle_tracking(image):
    results = model(image)

    # ตรวจสอบผลลัพธ์
    if not results:
        print("No results found")
        return []

    # ตรวจสอบผลลัพธ์
    if not results:
        print("No results found")
        return

    # สกัดข้อมูลการตรวจจับ
    detections = results[0].boxes.data.numpy()  # ใช้ `.boxes` สำหรับ YOLOv8

    # สร้าง array สำหรับเก็บพิกัดของกรอบสี่เหลี่ยมและข้อมูลอื่น ๆ
    detection_data = []

    height, width, _ = image.shape

    for detection in detections:
        x1, y1, x2, y2, confidence, class_id = detection[:6]
        label = model.names[int(class_id)]
        name_id = ""

        # กำหนดสีของกรอบ
        if label == "bus":
            color = (255, 0, 247)  # สีชมพู
            name_id = "b/b00" + str(int(class_id))
        elif label == "motorcycle":
            color = (251, 255, 0)  # สีฟ้า
            name_id = "s/m00" + str(int(class_id))
        elif label == "car":
            color = (0, 255, 0)  # สีเขียว
            name_id = "m/c00" + str(int(class_id))
        elif label == "truck":
            color = (0, 166, 255)  # สีส้ม
            name_id = "b/t00" + str(int(class_id))
        elif label == "bicycle":
            color = (0, 0, 255)
            name_id = "s/b00" + str(int(class_id))
        else:
            color = (64, 224, 208)  # สีแดง

        # วาดกล่องรอบๆ vehicle และ motorcycle (สำหรับการแสดงผล)
        if label in ["car", "truck", "bus", "motorcycle", "bicycle"]:  # ตรวจสอบว่าเป็น vehicle หรือ motorcycle
            cv2.rectangle(image, (int(x1), int(y1)), (int(x2), int(y2)), color, 2)
            cv2.putText(image, f"{label} {confidence:.2f}", (int(x1), int(y1) - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                        color, 2)

            # คำนวณพิกัด normalized vertices
            normalized_vertices = [
                {"x": x1 / width, "y": y1 / height},
                {"x": x2 / width, "y": y1 / height},
                {"x": x2 / width, "y": y2 / height},
                {"x": x1 / width, "y": y2 / height}
            ]

            # เก็บข้อมูลการตรวจจับ
            detection_data.append({
                "name": label,
                "mid": name_id,
                "score": float(confidence),
                "boundingPoly": {
                    "normalizedVertices": normalized_vertices
                }
            })

    # cv2.imshow('Vehicle and Motorcycle Tracking', image)
    # cv2.waitKey(0)

    # แปลงข้อมูลเป็น JSON
    # json_data = json.dumps(detection_data, indent=4)
    return detection_data


# เรียกใช้ฟังก์ชั่นสำหรับรูปภาพที่ต้องการ
# image_path = 'test_img/test1.jpeg'
# json_result = vehicle_tracking(image_path)


@app.route('/api/v1/ai/traffic_detections', methods=['POST', 'GET'])
def traffic_detections():
    if request.method == 'POST':
        if 'photo' not in request.files:
            return jsonify({"error": "No image provided"}), 400

        file = request.files['photo']
        if file.filename == '':
            return jsonify({"error": "No image selected for uploading"}), 400

        # อ่านรูปภาพจากไฟล์ที่อัพโหลด
        image = np.frombuffer(file.read(), np.uint8)
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)

        if image is None:
            return jsonify({"error": "Image file is invalid"}), 400

        # ตรวจจับ vehicle และ motorcycle
        detection_data = vehicle_tracking(image)
        print(detection_data)
        res = {
            "responses": [
                {"localizedObjectAnnotations": detection_data}
            ]
        }
        return res
    if request.method == 'GET':
        return "developing"

if __name__ == '__main__':
    app.run(debug=True)

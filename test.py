# ฝั่ง api
from ultralytics import YOLO
from flask import Flask, request, json

app = Flask(__name__)
model = YOLO("yolov9c.pt") 

@app.route('/api/v1/ai/traffic_detections', methods=['POST'])
def CreateAiTrafficDetection():
    print(request.files)
    body_file = dict(request.files)
    try:
        image_file = body_file['photo']
        image_filename = image_file.filename

        if not os.path.exists("assets/competition"): 
            os.makedirs("assets/competition") 

        with open('assets/competition/'+image_filename, "wb") as file:
            file.write(image_file.read())

        results = model(source='assets/competition/'+image_filename, conf=0.4, save=False, classes=[1, 2, 3, 5, 6, 7])
        names = model.names
        detect_point = []

        for r in results[0].boxes:
            # print("conf => {} and id => {}".format(r.conf, r.id))
            detect_point.append(
                {
                    "name": names[int(r.cls.item())],
                    "score": r.conf.item(),
                    "boundingPoly": {
                        "normalizedVertices": [
                            { "x": r.xyxyn[0][0].item(), "y": r.xyxyn[0][1].item() },
                            { "x": r.xyxyn[0][2].item(), "y": r.xyxyn[0][1].item() },
                            { "x": r.xyxyn[0][2].item(), "y": r.xyxyn[0][3].item() },
                            { "x": r.xyxyn[0][0].item(), "y": r.xyxyn[0][3].item() },
                        ]
                    }
                }
            )

        return {
            'responses': [
                {
                    "localizedObjectAnnotations": detect_point
                }
            ]
        }, 200
    
    except Exception as e:
        return {
            'message': e,
            'data': None
        }, 400